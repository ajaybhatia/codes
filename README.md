Codes
=====

This repo contains source code in multiple languages like:

1. Android
2. AngularJS
3. bash
4. C
5. C++
6. django
7. Java
8. JavaFX
9. jQuery
10. Javascript
12. Kernel modules
13. Matlab
14. node.js
15. node-webkit
16. Perl
17. Python
18. Qt
19. R
20. Ruby on Rails
21. Ruby

More source code from other languages is in progress.

Stay in touch!!!

Fork it/Clone it - Be a part of it.
